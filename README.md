# Enflux Pre-Interview Questions

## Diff
After cloning or downloading the zip file, cd into `diff` open a `sbt` session
and run

    > clean
    > run


## Social Network
After cloning or downloading the zip file, cd into `graph` open a `sbt` session
and run the following (the example CSV file is located at
`graph/src/test/resources/test.csv`)

    > clean
    > run "graph/src/test/resources/test.csv" 1
    > run "graph/src/test/resources/test.csv" 7

Once in sbt, you can also run `test` which includes test cases for the provided
examples.

Alternatively, a jar can also be produced and executed with Scala 2.12 by
running `sbt package` followed by `scala target/scala-2.12/graph_2.12-1.0.jar`

The expected arguments are the path to a CSV file and the ID of the post to
print the number of followers for.

    sbt run filepath id

or

    scala target/scala-2.12/graph_2.12-1.0.jar filepath id
