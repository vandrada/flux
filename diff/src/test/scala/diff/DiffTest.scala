import org.scalatest.FunSuite;
import org.scalatest.BeforeAndAfter;
import diff.Main;

class DiffTest extends FunSuite with BeforeAndAfter {
  var current: Array[Int] = _
  var target: Array[Int] = _

  before {
    current = Array(1, 3, 5, 6, 8, 9)
    target = Array(1, 2, 5, 7, 9)
  }

  test("provided examples") {
    val (add, del) = Main.diff(current, target)
    assert(add === Array(2, 7))
    assert(del === Array(3, 6, 8))
  }

  test("expanded example") {
    val (add, del) = Main.diff(current, target)
    // current + add - del
    val x = current.union(add).diff(del)
    assert(target.sorted.sameElements(x.sorted))
  }
}
