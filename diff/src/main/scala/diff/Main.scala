package diff;

object Main {
  def main(args: Array[String]) {
    val current = Array(1, 3, 5, 6, 8, 9)
    val target = Array(1, 2, 5, 7, 9)

    val (add, del) = diff(current, target)
    println(s"additions:[${format(add)}]")
    println(s"deletions are: [${format(del)}]")
  }

  def diff[T](current: Array[T], target: Array[T]): (Array[T], Array[T]) = {
    val additions = target.filter((x) => !current.contains(x))
    val deletions = current.filter((x) => !target.contains(x))

    (additions, deletions)
  }

  def format[T](arr: Array[T]): String = arr.mkString("", ", ", "")

}
