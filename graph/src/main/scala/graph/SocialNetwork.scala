package network;

import scala.collection.mutable.MutableList;

object SocialNetwork {
  abstract class Post(
    val id: Int,
    val followers: Int
  )

  case class NewPost(
    override val id: Int,
    override val followers: Int
  ) extends Post(id, followers)

  case class Repost(
    override val id: Int,
    val repostId: Int,
    override val followers: Int
  ) extends Post(id, followers)
}

/**
 * A model of a social network where the primrary object is a post.
 */
class SocialNetwork {
  import SocialNetwork.{Post, Repost, NewPost};

  val graph = new DirectedGraph[Post]

  /**
   * Adds a new post to the Network
   */
  def addPost(post: Post): Unit = {
    post match {
      case NewPost(_, _) => graph.addVertex(post)
      case Repost(_, repostId, _) => {
        for (parent <- getPost(repostId)) {
          graph.addVertex(post)
          graph.addEdge(parent, post)
        }
      }
    }
  }

  /**
   * Finds a post in the network
   */
  def getPost(id: Int): Option[Post] = {
    graph.findVertex((post) => post.id == id)
  }

  /**
   * Gets all the posts in the network
   */
  def getPosts(): List[Post] = {
    graph.getVertices
  }

  def countFollowers(postId: Int): Option[Int] = {
    for {
      post <- getPost(postId)
    } yield graph.reachable(post).map(_.followers).sum
  }

  override def toString: String = s"$graph"
}
