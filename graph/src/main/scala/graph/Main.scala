package network;

import scala.io.BufferedSource;
import scala.util.Try;
import java.io.IOException;
import java.io.FileNotFoundException;

import network.SocialNetwork.{Post, NewPost, Repost};

object Main {
  def main(args: Array[String]) {
    if (args.size < 2) {
      println("usage: [filename] [post id]")
      return
    }

    val filename = args(0)
    val postId = Try(args(1).toInt).toOption
    val network = new SocialNetwork

    if (!postId.isDefined) {
      println(s"post id must be numeric, was '${args(1)}'")
      return
    }

    val input = parseCsv(filename)
    input.foreach { (lines) =>
      for (line <- lines) {
        network.addPost(mkPostFromCsv(line))
      }

      val followers = network.countFollowers(postId.get)
      if (!followers.isEmpty) {
        println(s"post ${postId.get} has ${followers.get} followers")
      } else {
        println(s"post ${postId.get} doesn't have any followers")
      }
    }
  }

  def parseCsv(filename: String): Option[List[Array[Int]]] = {
    try {
      var buffered = io.Source.fromFile(filename)
      val values = buffered.getLines.drop(1).map {
        (line) => line.split(",").map {
          (str) => str.trim.toInt
        }
      }

      val result = Some(values.toList.filter(_.size == 3))
      buffered.close
      result
    } catch {
      case e: FileNotFoundException => {
        println(s"couldn't open file $filename")
        None
      }
      case e: IOException => {
        println(s"csv in $filename is malformed: $e")
        None
      }
    }
  }

  def mkPostFromCsv(input: Array[Int]): Post = {
    if (input(1) == -1) {
      NewPost(input(0), input(2))
    } else {
      Repost(input(0), input(1), input(2))
    }
  }
}
