package network;

import scala.collection.mutable.HashMap;
import scala.collection.mutable.MutableList;

/**
 * A simple directed graph adapted from Guido's implementation for Python
 *
 * A DirectedGraph is parametric over what the vertices contain.
 */
class DirectedGraph[T] {

  val vertices = HashMap[T, MutableList[T]]()

  /**
   * Adds a new vertex to the graph, the new vertex will have no edges
   */
  def addVertex(vertex: T): Unit = {
    vertices += (vertex -> MutableList())
  }

  /**
   * Connects two nodes in the graph
   */
  def addEdge(parent: T, child: T): Unit = {
    for {
      c <- getVertex(child)
      p <- vertices.get(parent)
    } yield p += child
  }

  /**
   * Gets the vertex containing `t`
   */
  def getVertex(t: T): Option[T] = vertices.keySet.find(_ == t)

  /**
   * Finds a vertex where the predicate `p` is true
   */
  def findVertex(p: T => Boolean): Option[T] = vertices.keys.find(p)

  /**
   * Gets all the edges directly connected to a vertex
   */
  def getEdges(vertex: T): Option[List[T]] = {
    for {
      vs <- vertices.get(vertex)
    } yield vs.toList
  }

  /**
   * Gets all the vertices in the graph
   */
  def getVertices(): List[T] = {
    vertices.keys.toList
  }

  /**
   * Gets all the vertices that can be reached from a vertex, since a vertext
   * can be reached by itself, it is included in the returned List
   */
  def reachable(vertex: T): List[T] = {
    def loop(vertex: T, acc: List[T]): List[T] = {
      getEdges(vertex).fold(acc)((children) => {
        children.foldLeft(vertex :: acc) {
          (result, current) => loop(current, result)
        }
      })
    }

    loop(vertex, List())
  }

  override def toString: String = s"$vertices"
}
