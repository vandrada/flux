import org.scalatest.FunSuite;
import org.scalatest.BeforeAndAfter;
import network.DirectedGraph;

class GraphTest extends FunSuite with BeforeAndAfter {
  var graph: DirectedGraph[Int] = _

  before {
    graph = new DirectedGraph[Int]()
  }

  test("graph can find inserted vertex") {
    graph.addVertex(1)
    assert(graph.findVertex((v) => v == 1) === Some(1))
    assert(graph.getVertex(1) === Some(1))
  }

  test("graph cannot find non-existent vertext") {
    assert(graph.getVertex(42) === None)
  }

  test("edges are created") {
    graph.addVertex(1)
    graph.addVertex(2)
    graph.addEdge(1, 2)
    assert(graph.getEdges(1) !== None)
  }

  test("reachable() returns root vertex") {
    graph.addVertex(34)
    assert(graph.reachable(34) === List(34))
  }

  test("reachable() returns direct children") {
    graph.addVertex(34)
    graph.addVertex(48)
    graph.addEdge(34, 48)
    val edges = graph.reachable(34)
    assert(edges.contains(34) && edges.contains(48) === true)
  }

  test("reachable() returns all descendents") {
    graph.addVertex(1)
    graph.addVertex(2)
    graph.addVertex(3)
    graph.addEdge(1, 2)
    graph.addEdge(2, 3)
    val edges = graph.reachable(1)
    assert(edges.contains(1) && edges.contains(2) && edges.contains(3) === true)
  }

  test("edge between two nodes requires the child to be in graph") {
    graph.addVertex(2)
    graph.addEdge(2, 23)
    assert(graph.getEdges(2) === Some(List()))
  }

  test("reachable on empty graph is empty list") {
    assert(graph.reachable(2) === List())
  }
}
