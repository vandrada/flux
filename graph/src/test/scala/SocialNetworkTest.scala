import org.scalatest.FunSuite;
import org.scalatest.BeforeAndAfter;
import network.SocialNetwork;
import network.SocialNetwork.{Post, Repost, NewPost};

class SocialNetworkTest extends FunSuite with BeforeAndAfter {
  var network: SocialNetwork = _

  before {
    network = new SocialNetwork()
  }

  /* addPost */
  test("new post is inserted") {
    network.addPost(NewPost(1, 300))
    assert(network.getPost(1) === Some(NewPost(1, 300)))
  }

  test("reposts are added to their parent post") {
    network.addPost(NewPost(1, 33))
    network.addPost(Repost(2, 1, 45))
    assert(network.getPosts().size === 2)
  }

  /* countFollowers */
  test("post with 0 followers returns 0") {
    network.addPost(NewPost(1, 0))
    assert(network.countFollowers(1) === Some(0))
  }

  test("followers of non-existent post is None") {
    assert(network.countFollowers(1) === None)
  }

  test("followers include reposts") {
    network.addPost(NewPost(1, 20))
    network.addPost(Repost(2, 1, 20))
    assert(network.countFollowers(1) === Some(40))
  }

  /* getPost */
  test("post not in network is None") {
    assert(network.getPost(2) === None)
  }

  /* getPosts */
  test("repost of non-existent post is not inserted") {
    network.addPost(Repost(2, 1, 30))
    assert(network.getPosts() === List())
  }
}
