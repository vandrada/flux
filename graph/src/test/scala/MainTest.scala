import org.scalatest.FunSuite;
import org.scalatest.BeforeAndAfter;

import scala.io.Source;

import network.SocialNetwork;
import network.SocialNetwork.{Post, Repost, NewPost};
import network.Main;

class MainTest extends FunSuite with BeforeAndAfter {

  var network: SocialNetwork = _

  before {
    network = new SocialNetwork
    val resource = getClass.getResource("test.csv").getPath
    val input = Main.parseCsv(resource)

    input.foreach { (lines) =>
      for (line <- lines) {
        network.addPost(Main.mkPostFromCsv(line))
      }
    }
  }

  test("post 1 has 350 followers") {
    assert(network.getPosts().size === 9)
    assert(network.countFollowers(1) === Some(350))
  }

  test("post 7 has 480 followers") {
    assert(network.getPosts().size === 9)
    assert(network.countFollowers(7) === Some(480))
  }

  test("imaginary post is None") {
    assert(network.countFollowers(10) === None)
  }
}
